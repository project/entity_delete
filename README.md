CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * FAQ
 * Maintainers


INTRODUCTION
------------

Entity Delete module is for deleting content of any entity types in bulk.


REQUIREMENTS
------------

Drupal 8 or 9 is required - Drupal 9 suggested.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See the
[Drupal 8 module install instructions](https://drupal.org/documentation/install/modules-themes/modules-8)
if required.


CONFIGURATION
-------------

For deleting content in a bulk need to navigate to `admin/config/entity-delete`
Select the Entity type for which you need to delete all available content.

Eg: If you select Entity type as node(Content in drop down), will get another
drop down to select content type. If you wish to delete all existing node
you can select 'All' else the any specific type.
Once selection is done just hit submit it will ask for confirmation, once
confirm it will delete all the entity based on existing selection.

FAQ
-----------

Any questions? Ask away on the issue queue.

Alternatively feel free to contact the maintainers.


MAINTAINERS
-----------

* Aditya Anurag - https://www.drupal.org/u/aditya_anurag
* Mahaveer Singh Panwar - https://www.drupal.org/u/mahaveer003
* A Ajay Kumar Reddy - https://www.drupal.org/u/ajay_reddy
* Sang Lostrie - https://www.drupal.org/u/baikho
* Henrique Mendes - https://www.drupal.org/u/hmendes
